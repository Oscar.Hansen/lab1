package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {

	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    boolean test1 = true;
    

    public void run() {
        while(true){
            System.out.println("Let's play round " + roundCounter +"!");
            String playermove = readInput("Your choice (Rock/Paper/Scissors)?");
            Random r = new Random();
            int randomitem = r.nextInt(rpsChoices.size());
            String computerMove = rpsChoices.get(randomitem);
            String whowins = myMethod(playermove, computerMove);
            System.out.println("Human chose " + playermove + ", computer chose " + computerMove + ". " + whowins);
            roundCounter++;
            if(whowins.equals("Human wins!")){
                humanScore++;
            }
            if(whowins.equals("Computer wins!")){
                computerScore++;
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            System.out.println("Do you wish to continue playing? (y/n)?");
            String try_again = sc.nextLine().toLowerCase();
            if(!try_again.equals("y")){
                System.out.println("Bye bye :)");
                break;
            
        }
        }
    }

    
     
    public String myMethod(String playermove, String computerMove) {
        if(computerMove.equals(playermove)) {
            return("It's a tie!");
        }
        else if(computerMove.equals("rock") && playermove.equals("paper")) {
            return("Human wins!");
        }
        else if(computerMove.equals("rock") && playermove.equals("scissors")) {
            return("Computer wins!");
        }
        else if(computerMove.equals("scissors") && playermove.equals("rock")) {
            return("Human wins!");
        }
        else if(computerMove.equals("paper") && playermove.equals("rock")) {
            return("Computer wins!");
        }
        else if(computerMove == "paper" && playermove == "scissors") {
            return("Human wins!");
        }
        else {
            return("Computer wins!");
        }
    }


    public boolean validateinput(String userInput) {
        if (rpsChoices.contains(userInput)){
            return true;
        }
        else {
            return false;
        }
    }

    public String readInput(String prompt) {
        while(true){
        System.out.println(prompt);
        String userInput = sc.nextLine();
        if (validateinput(userInput)){
        return userInput;
        }
        System.out.println("I do not understand " + userInput + ". Could you try again?");
        continue;
        }
    }

}
